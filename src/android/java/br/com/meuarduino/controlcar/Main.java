package br.com.meuarduino.controlcar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		
		System.out.println("******** start.1");
		try {
			System.out.println("******** start.2");
	        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
	        double width = visualBounds.getWidth();
	        double height = visualBounds.getHeight();
			System.out.println("******** start.3");

	        AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
			Scene scene = new Scene(root,width,height, true);
			System.out.println("******** start.4");
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
			System.out.println("******** start.5");

			stage.show();
			System.out.println("******** start.6");

		} catch(Exception e) {
			System.out.println("******** ERROR *****************************");
			e.printStackTrace();
		}
	}

	
//    @Override
//    public void start(Stage stage) throws Exception {
//        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
//        double width = visualBounds.getWidth();
//        double height = visualBounds.getHeight();
//
//        Sphere sphere = new Sphere(64.0);
//
//        Rectangle rectangle = new Rectangle(width - 20, height - 20);
//        rectangle.setFill(Color.BLUE);
//        rectangle.setArcHeight(6);
//        rectangle.setArcWidth(6);
//
//        StackPane stackPane = new StackPane();
//        stackPane.getChildren().addAll(rectangle, sphere);
//
//        Scene scene = new Scene(stackPane, visualBounds.getWidth(), visualBounds.getHeight());
//
//        stage.setScene(scene);
//        stage.show();
//    }

}

